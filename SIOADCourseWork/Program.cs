﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

namespace SIOADCourseWork
{
   
    class Program
    {
        public static double medLength = 0;
        public static double a1 = 8.41;

        private static List<Person> ReadBase(string[] baza, ref string s)
        {
            List<Person> sorted = new List<Person>();
            for(int i = 0; i < baza.Length; i++)
            {
                s += baza[i] + " ";
                string[] getString = baza[i].Split(' ');
                sorted.Add(new Person { Name = getString[0], Surname = getString[1], Patronymic = getString[2], Sum = getString[3], Date = getString[4], Lawyer = getString[5] + " " + getString[6] });
            }
            sorted = sorted.OrderBy(x => x.Name).ThenBy(x => x.Surname).ThenBy(x => x.Patronymic).ThenBy(x => x.Sum).ToList();


            return sorted;
        }

        public static void GetEntropy()
        {
            Console.WriteLine("Энтропия: " + (Math.Log(a1)/ Math.Log(2)) + 1.3);
        }
        private static void PrintBase(string[] baza)
        {
           
            int count = 0;
            for(int i = 0; i < baza.Length; i++)
            {
                Console.WriteLine(baza[i]);
                if (count == 20)
                {
                    count = 0;
                    Console.WriteLine("Ещё 20 y/n");
                    string contin = Console.ReadLine();
                    if (contin == "n")
                        break;

                    while (contin != "y")
                    {
                        Thread.Sleep(250);
                    }
                }
                count++;
            }
        }

        private static List<string> GetA1Elements(string line, List<string> vhozhdenia)
        {
            var groups = line.Where(char.IsLetterOrDigit).GroupBy(c => c);
            foreach (var g in groups)
                vhozhdenia.Add(string.Format("{1}:{0}", g.Key, g.Count()));

            return vhozhdenia;
        }

        static void Main(string[] args)
        {
            medLength = 4.265765765480875;
            Console.SetWindowSize(170, 50);
            Console.WriteLine("1.Вывод базы");
            Console.WriteLine("2.Поиск");
            Console.WriteLine("3.Кодировка");
            string[] baza = File.ReadAllLines("newbase3.txt");
            string s = string.Empty;


            //sorted = sorted.OrderBy(x => x.Name).ThenBy(x => x.Surname).ThenBy(x => x.Patronymic).ToList();// сортировка ФИО встроенным merge sort
            switch (int.Parse(Console.ReadLine()))
            {
                case 1:

                    PrintBase(baza);
                    break;

                case 2:

                    List<Person> sorted = ReadBase(baza, ref s);
                    Tree tree = new Tree();
                    List<string> vhozhdeniaA1 = new List<string>();
                    vhozhdeniaA1 = GetA1Elements(s, vhozhdeniaA1);

                    vhozhdeniaA1.Sort();
                    vhozhdeniaA1.Reverse();

                    //Console.ReadKey();
                    List<string> ElementsA1 = new List<string>();

                    for (int i = 0; i < vhozhdeniaA1.Count; i++)
                        ElementsA1.Add(vhozhdeniaA1[i].Split(':')[1]);
                    foreach (Person s1 in sorted)
                        tree.Insert(s1.Name + " " + s1.Surname + " " + s1.Patronymic + " " + s1.Sum + " " + s1.Date + " " + s1.Lawyer);

                        Console.WriteLine("Ключ");
                    tree.Search(Console.ReadLine());

                    break;

                case 3:
                    string input = File.ReadAllText("newbase3.txt");
                    HuffmanTree huffmanTree = new HuffmanTree();

                    
                    huffmanTree.Build(input);

                    // Кодировка
                    BitArray encoded = huffmanTree.Encode(input);

                    Console.Write("Encoded: ");
                    foreach (bool bit in encoded)
                    {
                        string result = (bit ? 1 : 0) + "";
                        Console.Write(result);
                       /* try
                        {
                            File.AppendAllText("newbaseEncoded.txt", result);
                        }
                        catch { }*/
                    }
                    Console.WriteLine();

                    // Раскодировка
                    string decoded = huffmanTree.Decode(encoded);

                    Console.WriteLine("Decoded: " + decoded);

                    GetEntropy();
                    Console.WriteLine("Средняя длина кода" + medLength);
                   /* try
                    {
                        File.WriteAllText("newbaseDecoded.txt", decoded);
                    }
                    catch { }*/
                    break;
            }


            Console.ReadKey();
        }
    }
    class Person
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Patronymic { get; set; }
        public string Sum { get; set; }
        public string Date { get; set; }
        public string Lawyer { get; set; }
    }
}
