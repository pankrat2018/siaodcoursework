﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SIOADCourseWork
{
    class Tree
    {
        private string value;
        private int count;
        private Tree left;
        private Tree right;

        // вставка
        public void Insert(string value)
        {
            if (this.value == null)
                this.value = value;
            else
            {
                if (this.value.CompareTo(value) == 1)
                {
                    if (left == null)
                        left = new Tree();
                    left.Insert(value);
                }
                else if (this.value.CompareTo(value) == -1)
                {
                    if (right == null)
                        right = new Tree();
                    right.Insert(value);
                }

            }

            count = Recount(this);
        }
        // поиск
        public Tree Search(string value)
        {
            if (this.value.Contains(value))
            {
                Console.WriteLine(this.value);
                this.value = "";
                Search(value);
                return this;
            }
                
            else if (this.value.CompareTo(value) == 1)
            {
                if (left != null)
                    return left.Search(value);
                else
                    Console.WriteLine("Не найдено");
                return null;
            }
            else
            {
                if (right != null)
                    return right.Search(value);
                else
                    Console.WriteLine("Не найдено");
                return null;
            }
        }

        // отображение в строку
        public string Display(Tree t)
        {
            string result = "";
            if (t.left != null)
                result += Display(t.left);

            result += t.value + " ";

            if (t.right != null)
                result += Display(t.right);

            return result;
        }
        // подсчет
        private int Recount(Tree t)
        {
            int count = 0;

            if (t.left != null)
                count += Recount(t.left);

            count++;

            if (t.right != null)
                count += Recount(t.right);

            return count;
        }
        // очистка
        public void Clear()
        {
            value = null;
            left = null;
            right = null;
        }
        // проверка пустоты
        public bool IsEmpty()
        {
            if (value == null)
                return true;
            else
                return false;
        }
    }
}
